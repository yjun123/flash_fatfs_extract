#!/bin/bash
# Copyright (C), 2015-2023, Sunny OIT. CO., Ltd.
# Filename: flash_fatfs_extract.sh
# Author: yanjun (zngyanj@sunnyoptical.com)
# Date: 2023/01/06
# Description: APE1210文件系统提取脚本
# Usage: ./flash_fatsfs_extract.sh --help

LOG_LEVEL=1

function log_info(){
  content="[INFO]: $(date '+%Y-%m-%d %H:%M:%S') $@"
  [ $LOG_LEVEL -le 1  ] && echo -e "\033[32m"  ${content} "\033[0m"
}

function log_warn(){
  content="[WARN]: $(date '+%Y-%m-%d %H:%M:%S') $@"
  [ $LOG_LEVEL -le 2  ] && echo -e "\033[33m"  ${content} "\033[0m"
}

function log_err(){
  content="[ERR]: $(date '+%Y-%m-%d %H:%M:%S') $@"
  [ $LOG_LEVEL -le 3  ] && echo -e "\033[31m"  ${content} "\033[0m"
}


function usage(){
	echo "Usage: $0 [FILE] [OPTION]"
	echo "-h, --help	show usage"
	echo "-c, --compress	compress files in flash fs binary instead of dumping, default is False"
	echo "-l, --list 	list files in flash fs binary, default is False"
	echo "-d, --dump 	dump files in flash fs binary to dirctory, default is True"
	exit -0
}

d_flag="true"
args=("$@")
nargs="$#"
# echo $nargs
[ "$nargs" -lt 1 ] && log_err "Must specify flash fs binary !" && usage $0
[ "$nargs" -gt 2 ] && log_err "Too much options !" && usage $0
[ "${args[0]}" == '-h' -o "${args[0]}" == "--help" ] && usage $0
if [ "$nargs" -gt 1 ]; then
	for arg in "${args[*]:1}"; do
		case $arg in
			"-c"|"--compress")
				c_flag="true"
			;;
			"-l"|"--list")
				l_flag="true"
			;;
			"-h"|"--help")
				usage $0
			;;
			*)
				log_err "Unkown option $arg !"
				exit -1
			;;
		esac
	done
else
	if [ ! -f "$1" ]; then
		log_err "$1 is not exist or not file !"
		exit -1
	fi
fi

FLASH_FATFS_BIN="${1}"
FLASH_FATFS_BIN_NAME=$(basename ${FLASH_FATFS_BIN})
TEMP_DIR=/tmp/extract-tmp

function main(){
  log_warn "Start to extract ${FLASH_FATFS_BIN} ...\n"
  
  loopname=$(losetup -f)
  if [ ! "$loopname" ]; then 
    log_err "No usable loop device."
    exit -1
  fi
  log_info "${loopname} is usable."

  losetup -b 4096 -P "${loopname}" "${FLASH_FATFS_BIN}"
  if [ $? -ne 0 ]; then
    log_err "Set ${FLASH_FATFS_BIN_NAME} as loop device Failed !"
    exit -1
  fi
  log_info "Set ${FLASH_FATFS_BIN_NAME} as loop device."

  mkdir -p "${TEMP_DIR}"
  
  umount ${TEMP_DIR} > /dev/null 2>&1
  mount -t vfat ${loopname}p1 ${TEMP_DIR}
  if [ $? -ne 0 ]; then
    log_err "Mount ${loopname}p1 to ${TEMP_DIR} Failed!"
    losetup -d $loopname
    exit -1
  fi

  log_info "Mount ${loopname}p1 to ${TEMP_DIR}."

  if [ "$l_flag" == "true" ]; then # list files
	cd "$TEMP_DIR"
	log_info "List files in ${FLASH_FATFS_BIN_NAME}:"
	find *
	cd /tmp
	# tree ${TEMP_DIR}
    	umount ${TEMP_DIR}
    	losetup -d $loopname
	exit 0
  fi

  if [ "$c_flag" == "true" ]; then # compress to archive(tar.gz)
	archive_file=${PWD}/${FLASH_FATFS_BIN_NAME%.bin}.tar.gz
  	rm -rf "$archive_file" && \
	tar -czvf "$archive_file" -C ${TEMP_DIR}/ .
  	if [ $? -ne 0 ]; then
    		log_err "Extract ${FLASH_FATFS_BIN_NAME} Failed!\n"
    		umount ${TEMP_DIR}
    		losetup -d $loopname
    		exit -1
	fi
  	log_info "Extract ${FLASH_FATFS_BIN} to ${PWD}/${FLASH_FATFS_BIN_NAME%.bin}.tar.gz!\n"
  else # dump to dirctory
	dump_dir="${PWD}/${FLASH_FATFS_BIN_NAME%.bin}"
	mkdir -p "$dump_dir" && \
	rm -rf "$dump_dir"/* && \
	cp -r ${TEMP_DIR}/* "$dump_dir"
	chown -R $SUDO_UID:$SUDO_GID "$dump_dir"
	# find "$dump_dir" -type f -exec chmod 0644 {} \;
	find "$dump_dir" -type d -exec chmod 0755 {} \;
  	if [ $? -ne 0 ]; then
    		log_err "Extract ${FLASH_FATFS_BIN_NAME} Failed!\n"
    		umount ${TEMP_DIR}
    		losetup -d $loopname
    		exit -1
	fi
  	log_info "Extract ${FLASH_FATFS_BIN} to "$dump_dir"/ !\n"
  fi
  
  sync
  umount ${TEMP_DIR}
  
  sync
  losetup -d $loopname

  log_warn "Extract ${FLASH_FATFS_BIN} successfully!"
}

if [ $UID -ne 0 ];then
  log_err "only root can do that!"
  exit
fi
main $1
