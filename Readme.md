### Flash_fatfs_extract

#### 介绍

- 视海芯图APE1210 文件系统提取脚本

#### 功能

- 提取文件系统中的文件到当前目录

  ```shell
  $ sudo ./flash_fatfs_extract.sh flash_fatfs_npu_3000000.bin 
  ```

- 提取并打包文件系统的文件到当前目录

  ```shell
  $ sudo ./flash_fatfs_extract.sh flash_fatfs_npu_3000000.bin -c
  ```

- 查看文件系统中的文件内容

  ```shell
  $ sudo ./flash_fatfs_extract.sh flash_fatfs_npu_3000000.bin -l
  ```

#### 已知问题

- 在ubuntu 20上可以正常使用，ubuntu18上无法执行挂载(mount)，报错如下
  

 ```shell
$ sudo ./flash_fatfs_extract.sh flash_fatfs_npu_3000000.bin                                                                                                 
[WARN]: 2023-01-07 15:10:04 Start to extract flash_fatfs_npu_3000000.bin ...

[INFO]: 2023-01-07 15:10:04 /dev/loop34 is usable. 
[INFO]: 2023-01-07 15:10:04 Set flash_fatfs_npu_3000000.bin as loop device. 
mount: /tmp/extract-tmp: wrong fs type, bad option, bad superblock on /dev/loop34p1, missing codepage or helper program, or other error.
[ERR]: 2023-01-07 15:10:04 Mount /dev/loop34p1 to /tmp/extract-tmp Failed!
 ```

 ```
内核打印：
[609039.030185]  loop34: p1
[609039.082126] FAT-fs (loop34p1): bogus number of reserved sectors
[609039.082128] FAT-fs (loop34p1): Can't find a valid FAT filesystem
 ```
